package network;

import java.io.Serializable;

import network.inteface.NetCommandInteface;

public class Control implements NetCommandInteface, Serializable {

	private Integer command = -1;
	////
	public static final Integer CLOSE = 0;
	public static final Integer READ_SENSORS = 1;
	//public static final Integer READ_TEMPERATURE = 2;
	public static final Integer READ_CONTROLS = 3;
	public static final Integer SWITCH_LIGHT = 4;
	public static final Integer SWITCH_RADIATOR = 5;
	public static final Integer ADJUST_RADIATOR = 6;
	public static final Integer LED = 7;
	public static final Integer READ_NOTES = 8;
	public static final Integer ADD_NOTE = 9;
	// public static final Integer = ;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public Integer getCommand() {
		return command;
	}

	@Override
	public void setCommand(Integer command) {
		this.command = command;
	}

}
