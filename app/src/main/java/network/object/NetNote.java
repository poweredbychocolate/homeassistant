package network.object;

import java.io.Serializable;

public class NetNote implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String author;
	private String content;
	
	public NetNote(){}
	public NetNote(String author, String content) {
		super();
		this.author = author;
		this.content = content;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	

}
