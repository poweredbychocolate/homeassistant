package network.object;

import java.io.Serializable;

import network.inteface.NetSensor;

public class NetTemperatureSensor implements NetSensor,Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name;
	private double temperature;

	public NetTemperatureSensor(String name,double temperature) {
		this.name=name;
		this.temperature=temperature;
	}	
	
	@Override
	public void setName(String name) {
		this.name=name;

	}

	public void setValue(double value) {
		this.temperature=value;

	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return this.name;
	}


	public double getValue() {
		// TODO Auto-generated method stub
		return this.temperature;
	}

}
