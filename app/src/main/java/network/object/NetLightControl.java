package network.object;

import java.io.Serializable;

import network.inteface.NetControl;

public class NetLightControl implements NetControl, Serializable{
	private String name;
	private boolean state;
	
	public NetLightControl() {}

	public NetLightControl(String name,Boolean state) {
		this.name = name;
		this.state = state;
	}


	private static final long serialVersionUID = 1L;

	public void setName(String name) {
		this.name = name;

	}

	public void setState(boolean state) {
		this.state = state;

	}

	public String getName() {
		return this.name;
	}

	public boolean getState() {
		return this.state;
	}

}
