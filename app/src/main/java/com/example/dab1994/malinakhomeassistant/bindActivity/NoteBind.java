package com.example.dab1994.malinakhomeassistant.bindActivity;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.example.dab1994.malinakhomeassistant.R;
import com.example.dab1994.malinakhomeassistant.WorkActivity;
import com.example.dab1994.malinakhomeassistant.database.Database;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;

import network.Control;
import network.object.NetNote;

public class NoteBind extends AppCompatActivity {
    private Database db;
    private InputMethodManager imm;
    ////
    private CardView innerCard,outerCard;
    private EditText newNoteContent,newNoteAutor;
    private FloatingActionButton newNoteSaveButton;
    ///since version 0.2
    private String serverAdress;
    private int serverPort;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_bind);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Bundle budle = getIntent().getExtras();
        serverAdress = budle.getString(WorkActivity.SERVER_ADRESS);
        serverPort = budle.getInt(WorkActivity.SERVER_PORT);



        db = Database.getInstance(this);
        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        ////
        outerCard=(CardView) findViewById(R.id.note_new_card_view);
        outerCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                   showKeyboard();
            }
        });
       innerCard = (CardView) findViewById(R.id.note_new_inner_card_view);
       innerCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showKeyboard();
            }
        });
        newNoteAutor = (EditText) findViewById(R.id.note_new_author);
        newNoteAutor.setText(db.getUserName(getIntent().getExtras().getLong(WorkActivity.Server_Id)));
        newNoteContent = (EditText) findViewById(R.id.note_new_content);
        ////
        newNoteSaveButton = (FloatingActionButton) findViewById(R.id.note_new_add);
        newNoteSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              // db.insertNote(newNoteAutor.getText().toString(), newNoteContent.getText().toString());
                new AddNote(newNoteAutor.getText().toString(), newNoteContent.getText().toString()).start();
               Toast.makeText(v.getContext(), R.string.notes_add_note, Toast.LENGTH_LONG).show();
                finish();
            }
        });
    }
            @Override
            public void onResume() {
                super.onResume();
                newNoteContent.setText(null);
                showKeyboard();
            }
            public void closeKeyboard()
            {
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
            }
            public void showKeyboard() {
                newNoteContent.requestFocus();
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
            }

    class AddNote extends Thread {
        private Socket socket = null;
        private ObjectOutputStream obOut;
        private ObjectInputStream obIn;
        private Control cmd;
        private String author,text;

        AddNote(String author,String text)
        {
            this.author=author;
            this.text=text;
        }
        @Override
        public void run() {
            try {
                Log.i("AddNote", "Run start");
                /// open connection and innit stream
                this.socket = new Socket(serverAdress, serverPort);
                this.obOut = new ObjectOutputStream(socket.getOutputStream());
                this.obIn = new ObjectInputStream(socket.getInputStream());
                /// send command add note
                cmd = new Control();
                cmd.setCommand(Control.ADD_NOTE);
                this.obOut.writeObject(cmd);
                this.obOut.flush();
                /// sent netnote
                NetNote n = new NetNote(author,text);
                this.obOut.writeObject(n);
                this.obOut.flush();
                ///send close command to server
                cmd = new Control();
                cmd.setCommand(Control.CLOSE);
                this.obOut.writeObject(cmd);
                this.obOut.flush();

                Log.i("AddNote", "Run end");

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    } /// read control thread
    }

