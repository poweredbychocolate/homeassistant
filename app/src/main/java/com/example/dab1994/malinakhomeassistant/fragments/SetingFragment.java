package com.example.dab1994.malinakhomeassistant.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.dab1994.malinakhomeassistant.R;
import com.example.dab1994.malinakhomeassistant.WorkActivity;
import com.example.dab1994.malinakhomeassistant.database.Database;

/**
 * A simple {@link Fragment} subclass.
 */
public class SetingFragment extends Fragment {
    private Button clearDBButton;

    public SetingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_seting, container, false);
        clearDBButton = (Button) view.findViewById(R.id.setting_remove_notes);
        clearDBButton.setOnClickListener(clearCL);
        return view;
    }
        View.OnClickListener clearCL =new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Database.getInstance(v.getContext()).deleteAllNote();
              Toast.makeText(v.getContext(),R.string.setting_clear_notes,Toast.LENGTH_LONG).show();
            }
        };

}
