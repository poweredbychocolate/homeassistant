package com.example.dab1994.malinakhomeassistant.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.dab1994.malinakhomeassistant.R;
import com.example.dab1994.malinakhomeassistant.WorkActivity;
import com.example.dab1994.malinakhomeassistant.bindActivity.NoteBind;
import com.example.dab1994.malinakhomeassistant.database.Database;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotesFragment extends Fragment {
    private RecyclerView notesRecyclerView;
    private RecyclerView.Adapter notesAdapter;
    private RecyclerView.LayoutManager notesLayoutManager;
    private Database db;
    ////
    private FloatingActionButton newNoteShowButton;
    /// since version 0.2

    public NotesFragment() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notes,container,false);
        db =Database.getInstance(getActivity());
        ////
        notesRecyclerView = (RecyclerView) view.findViewById(R.id.notes_view);
        notesLayoutManager = new LinearLayoutManager(getActivity());
        notesRecyclerView.setLayoutManager(notesLayoutManager);
        notesRecyclerView.setItemAnimator(new DefaultItemAnimator());
        newNoteShowButton = (FloatingActionButton) view.findViewById(R.id.note_new_show);
        newNoteShowButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent noteBindIntent =new Intent(getActivity(), NoteBind.class);
                noteBindIntent.putExtra(WorkActivity.Server_Id,((WorkActivity)getActivity()).getServerId());
                noteBindIntent.putExtra(WorkActivity.SERVER_ADRESS,((WorkActivity)getActivity()).getServerAdress());
                noteBindIntent.putExtra(WorkActivity.SERVER_PORT, ((WorkActivity)getActivity()).getServerPort());
                startActivity(noteBindIntent);
                }
        });
       ////
      //  insertExaplesNotes();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
       // notesAdapter = new NotesAdapter(db.getCursor(Database.NOTES_CURSOR));
       // notesRecyclerView.setAdapter(notesAdapter);
        notesAdapter = new NotesNetworkAdapter(((WorkActivity)getActivity()).getServerAdress(),((WorkActivity)getActivity()).getServerPort());
        notesRecyclerView.setAdapter(notesAdapter);
           }
    public void insertExaplesNotes()
    {
        if(db.getCursor(Database.NOTES_CURSOR).getCount()<8){
        db.insertNote("Arduino","Witaj,\nInstalacja systemu zakończona \nTwoje Arduino");
        db.insertNote("Domownik2","Zakupy,\nMleko,\nMasło\nChleb");
        db.insertNote("Arduino","Awaria, \nWykryto nieprawidlowosci w funkcjonowaniu czujnika\nID : D21\nNazwa : dzwi boczne");
        db.insertNote("Domownik1","Wracam dzisiaj do póżniej.\nBędę około 20:02");
        }
    }
  }
