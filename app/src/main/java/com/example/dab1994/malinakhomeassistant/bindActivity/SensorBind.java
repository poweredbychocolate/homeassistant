package com.example.dab1994.malinakhomeassistant.bindActivity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.example.dab1994.malinakhomeassistant.R;

public class SensorBind extends AppCompatActivity {
    private Spinner sensorSelected;
    private  ArrayAdapter<String> sensorAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensor_bind);
        sensorSelected = (Spinner) findViewById(R.id.sensor_selected);
        /// !!!! move to onStart
           sensorAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,new String[]{"Sensor01","Sensor02","Sensor03"});
            sensorAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sensorSelected.setAdapter(sensorAdapter);
        /// !!!! move to onStart
    }
}
