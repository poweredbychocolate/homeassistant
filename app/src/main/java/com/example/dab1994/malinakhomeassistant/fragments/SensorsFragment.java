package com.example.dab1994.malinakhomeassistant.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.dab1994.malinakhomeassistant.R;
import com.example.dab1994.malinakhomeassistant.WorkActivity;
import com.example.dab1994.malinakhomeassistant.bindActivity.SensorBind;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import network.Control;


/**
 * A simple {@link Fragment} subclass.
 */
public class SensorsFragment extends Fragment {
    private RecyclerView sensorRecyclerView;
    private SensorNetworkAdapter sensorAdapter;
    private RecyclerView.LayoutManager sensorLayoutManager;
    private FloatingActionButton newSensorShowButton;

    //since version 0.2
    public SensorsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sensors, container, false);
        sensorRecyclerView = (RecyclerView) view.findViewById(R.id.sensors_view);
        sensorLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        sensorRecyclerView.setLayoutManager(sensorLayoutManager);
        sensorRecyclerView.setItemAnimator(new DefaultItemAnimator());
        newSensorShowButton = (FloatingActionButton) view.findViewById(R.id.sensors_new_show);
        newSensorShowButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), SensorBind.class));
            }
        });

        /// since version 0.2

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        sensorAdapter = new SensorNetworkAdapter(((WorkActivity)getActivity()));
          sensorRecyclerView.setAdapter(sensorAdapter);
    }

    @Override
    public void onStop() {
        super.onStop();
        sensorAdapter.shutdown();
    }
    WorkActivity getWorkActivity()
    {
        return (WorkActivity) this.getActivity();
    }
}
