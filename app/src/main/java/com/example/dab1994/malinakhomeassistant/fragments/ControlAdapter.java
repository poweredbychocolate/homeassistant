package com.example.dab1994.malinakhomeassistant.fragments;

import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

import com.example.dab1994.malinakhomeassistant.R;
import com.example.dab1994.malinakhomeassistant.WorkActivity;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.HashMap;
import java.util.Random;

import network.Control;
import network.object.NetLightControl;

/**
 * Created by dab1994 on 23.04.17.
 */

public class ControlAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private String[] names;
    private Integer[] types;
    private Random rand;

    /**
     * Type for control_temp.xml
     */
    public static final int TEMP = 0;
    /**
     * Type for control_switch.xml
     */
    public static final int SWITCH = 1;
    /**
     * Type for control_led.xml
     */
    public static final int RGB_LED = 2;

    /**
     * Create adapter using with ControlFragment
     *
     * @param names table of names control unit
     * @param types table of types above units
     */
    public ControlAdapter(String[] names, Integer[] types) {
        this.names = names;
        this.types = types;
        rand = new Random();
    }

    /**
     * Create view dependent on view type
     *
     * @param parent
     * @param viewType
     * @return View associated with type or null
     */
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case SWITCH: {
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.control_switch, parent, false);
                return new SwitchViewHolder(view);
            }
            case TEMP: {
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.control_temp, parent, false);
                return new TempViewHolder(view);
            }
            case RGB_LED: {
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.control_led, parent, false);
                return new LEDViewHolder(view);
            }
        }
        return null;
    }

    /**
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (types[position]) {
            case SWITCH: {
                ((SwitchViewHolder) holder).nameTextView.setText(names[position]);
                ((SwitchViewHolder) holder).cardView.setOnClickListener(switchCL);
                break;
            }
            case TEMP: {
                ((TempViewHolder) holder).nameTextView.setText(names[position]);
                int randVal = rand.nextInt(100);
                ((TempViewHolder) holder).tempTextView.setText(Integer.toString(randVal) + WorkActivity.CELSIUS);
                ((TempViewHolder) holder).tempSeekBar.setProgress(randVal);
                ((TempViewHolder) holder).tempSeekBar.setOnSeekBarChangeListener(new TempChangeListener(((TempViewHolder) holder).cardView));
                ((TempViewHolder) holder).tempSwitch.setOnCheckedChangeListener(new SwitchCheckListener(((TempViewHolder) holder).cardView));
                break;
            }
            case RGB_LED: {
                ((LEDViewHolder) holder).nameTextView.setText(names[position]);
                int[] randLED = {rand.nextInt(255), rand.nextInt(255), rand.nextInt(255)};
                ((LEDViewHolder) holder).colorCardView.setCardBackgroundColor(Color.rgb(randLED[0], randLED[1], randLED[2]));
                ((LEDViewHolder) holder).RSeekBar.setProgress(randLED[0]);
                ((LEDViewHolder) holder).GSeekBar.setProgress(randLED[1]);
                ((LEDViewHolder) holder).BSeekBar.setProgress(randLED[2]);
                LEDChangeListener ledCL = new LEDChangeListener(((LEDViewHolder) holder).colorCardView, ((LEDViewHolder) holder).RSeekBar, ((LEDViewHolder) holder).GSeekBar, ((LEDViewHolder) holder).BSeekBar);
                ((LEDViewHolder) holder).RSeekBar.setOnSeekBarChangeListener(ledCL);
                ((LEDViewHolder) holder).GSeekBar.setOnSeekBarChangeListener(ledCL);
                ((LEDViewHolder) holder).BSeekBar.setOnSeekBarChangeListener(ledCL);
                break;
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        return types[position];
    }

    @Override
    public int getItemCount() {
        return names.length;
    }

    /**
     * ViewHolder for control_temp
     *
     * @see android.support.v7.widget.RecyclerView.ViewHolder
     */
    public class TempViewHolder extends RecyclerView.ViewHolder {
        private TextView nameTextView, tempTextView;
        private SeekBar tempSeekBar;
        private CardView cardView;
        private Switch tempSwitch;

        /**
         * create ViewHolder for control_temp
         *
         * @param view
         */
        public TempViewHolder(View view) {
            super(view);
            nameTextView = (TextView) view.findViewById(R.id.control_temp_name);
            tempTextView = (TextView) view.findViewById(R.id.control_temp_celsius);
            tempSeekBar = (SeekBar) view.findViewById(R.id.control_temp_seek);
            tempSwitch = (Switch) view.findViewById(R.id.control_temp_switch);
            cardView = (CardView) view;
        }
    }
    //ViewHolders

    /**
     * ViewHolder for control_switch
     *
     * @see android.support.v7.widget.RecyclerView.ViewHolder
     */
    public class SwitchViewHolder extends RecyclerView.ViewHolder {
        private TextView nameTextView;
        private CardView cardView;

        /**
         * Create ViewHolder for control_switch
         *
         * @param view
         */
        public SwitchViewHolder(View view) {
            super(view);
            nameTextView = (TextView) view.findViewById(R.id.control_switch_name);
            cardView = (CardView) view;
        }
    }

    /**
     * ViewHolder for control_led
     *
     * @see android.support.v7.widget.RecyclerView.ViewHolder
     */
    public class LEDViewHolder extends RecyclerView.ViewHolder {
        private TextView nameTextView;
        private SeekBar RSeekBar, GSeekBar, BSeekBar;
        private CardView colorCardView;

        /**
         * Create ViewHolder for control_led
         *
         * @param view
         */
        public LEDViewHolder(View view) {
            super(view);
            nameTextView = (TextView) view.findViewById(R.id.control_led_name);
            colorCardView = (CardView) view.findViewById(R.id.control_led_color_card);
            RSeekBar = (SeekBar) view.findViewById(R.id.control_led_R);
            GSeekBar = (SeekBar) view.findViewById(R.id.control_led_G);
            BSeekBar = (SeekBar) view.findViewById(R.id.control_led_B);
        }
    }

    // action listerners
    private View.OnClickListener switchCL = new View.OnClickListener() {
        TextView text;

        @Override
        public void onClick(View v) {
            text = (TextView) v.findViewById(R.id.control_switch_state);

            if (text.getText().equals("ON")) {
                text.setText("OFF");
                text.setBackgroundResource(R.color.colorCardViewBackground);
            } else {
                text.setText("ON");
                text.setBackgroundResource(R.color.colorRGBG);
            }
        }
    };

    private class TempChangeListener implements SeekBar.OnSeekBarChangeListener {
        private View v;
        private TextView value;
        private Switch aSwitch;

        public TempChangeListener(View view) {
            this.v = view;
            this.value = (TextView) this.v.findViewById(R.id.control_temp_celsius);
            this.aSwitch = (Switch) v.findViewById(R.id.control_temp_switch);
        }

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            value.setText(Integer.toString(progress) + WorkActivity.CELSIUS);
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
            v.setBackgroundResource(R.color.colorCardViewBackground);
            aSwitch.setChecked(false);
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            //value.setText(Integer.toString(seekBar.getProgress()));
        }
    }

    private class SwitchCheckListener implements CompoundButton.OnCheckedChangeListener {
        private CardView v;

        public SwitchCheckListener(CardView view) {
            this.v = view;
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked)
                v.setBackgroundResource(R.color.colorRGBG);
            else
                v.setBackgroundResource(R.color.colorCardViewBackground);
        }
    }

    private class LEDChangeListener implements SeekBar.OnSeekBarChangeListener {
        private CardView cardRGB;
        private SeekBar rBar, gBar, bBar;

        public LEDChangeListener(CardView view, SeekBar r, SeekBar g, SeekBar b) {
            this.cardRGB = view;
            this.rBar = r;
            this.gBar = g;
            this.bBar = b;
        }

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            switch (seekBar.getId()) {
                case R.id.control_led_R: {
                    cardRGB.setCardBackgroundColor(Color.rgb(progress, gBar.getProgress(), bBar.getProgress()));
                    break;
                }
                case R.id.control_led_G: {
                    cardRGB.setCardBackgroundColor(Color.rgb(rBar.getProgress(), progress, bBar.getProgress()));
                    break;
                }
                case R.id.control_led_B: {
                    cardRGB.setCardBackgroundColor(Color.rgb(rBar.getProgress(), gBar.getProgress(), progress));
                    break;
                }
            }
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            //value.setText(Integer.toString(seekBar.getProgress()));
        }
    }



}