package com.example.dab1994.malinakhomeassistant.database;

import android.provider.BaseColumns;
import android.widget.SeekBar;

/**
 * Created by dab1994 on 18.04.17.
 */

public final class DBNotes implements BaseColumns {
    // public static final String = "";
    public static final String TABLE_NAME = "OfflineNotes";
    public static final String AUTHOR = "Author";
    public static final String CONTENTS = "Content";
    public static final int UNREAD = 0;
    public static final int READ = 1;
    /*
    This field mark note status
    0= new unread note.
    1 = means read notes
     */
    public static final String STATUS = "State";

    public static final String CREATE_TABLE = "CREATE TABLE " + DBNotes.TABLE_NAME + " (" + DBNotes._ID + " INTEGER PRIMARY KEY AUTOINCREMENT , "
            + DBNotes.AUTHOR + " TEXT, " + DBNotes.CONTENTS + " TEXT, " + DBNotes.STATUS + " INTEGER)";
    public static final String DROP = "DROP TABLE " + DBNotes.TABLE_NAME;
    public static final String SELECT_ALL = "SELECT " + DBNotes._ID + ", " + DBNotes.AUTHOR + ", " + DBNotes.CONTENTS + ", " + DBNotes.STATUS +
            " FROM " + DBNotes.TABLE_NAME + " ORDER BY " + DBNotes.STATUS + " ASC, " + DBNotes._ID + " DESC ";
    public static final String DELETE_CLAUSE = DBNotes._ID + " = ?";
    public static final String UPDATE_STATUS_CLAUSE = DBNotes._ID + " = ? ";
    public static final String DELETE_ALL = "DELETE FROM " + DBNotes.TABLE_NAME;
}
