package com.example.dab1994.malinakhomeassistant.database;

import android.provider.BaseColumns;

/**
 * Created by artu1 on 29.05.2017.
 */

public class DBSettings implements BaseColumns {
    ///
    public static final String TABLE_NAME = "AppSettings";
    public static final String TABLE_OPTIONS = "SettingOption";
    public static final String TABLE_VALUE = "SettingValue";
    ///
    public static final String NOTE_AUTODELETE= "NOTE_AUTOREMOVE";
    public static final String NOTE_VISABLE= "NoteVisable";
    public static final String SENSOR_VISABLE= "SensorVisable";
    public static final String CONTROL_VISABLE= "ControlVisable";
    ////
    public static final int DISABLE= 0;
    public static final int ENABLE= 1;
    public static final int ON_LEFT= 2;
    public static final int ON_RIGHT= 3;
    /**
     * Create table query
     */
    public static final String CREATE_TABLE =
            "CREATE TABLE " + DBSettings.TABLE_NAME + " (" + DBSettings._ID + " INTEGER PRIMARY KEY AUTOINCREMENT , "
                    + DBSettings.TABLE_OPTIONS + " TEXT, " + DBSettings.TABLE_VALUE + " INTEGER ) ";

    /**
     * Drop table query
     */
    public static final String DROP_TABLE = "DROP TABLE IF EXISTS " + DBSettings.TABLE_NAME;
    public static final String UPDATE_CLAUSE = DBSettings.TABLE_OPTIONS + " = ? ";
    public static final String SELECT_NOTE_VISABLE = "SELECT " + DBSettings.TABLE_VALUE +" FROM " + DBSettings.TABLE_NAME + " WHERE " + DBSettings.TABLE_OPTIONS + " = "+DBSettings.NOTE_VISABLE;
}