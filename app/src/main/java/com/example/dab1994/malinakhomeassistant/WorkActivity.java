package com.example.dab1994.malinakhomeassistant;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.example.dab1994.malinakhomeassistant.database.Database;
import com.example.dab1994.malinakhomeassistant.fragments.ControlFragment;
import com.example.dab1994.malinakhomeassistant.fragments.MySwipeFragmentAdapter;
import com.example.dab1994.malinakhomeassistant.fragments.NotesFragment;
import com.example.dab1994.malinakhomeassistant.fragments.SensorsFragment;
import com.example.dab1994.malinakhomeassistant.fragments.SetingFragment;

import java.util.ArrayList;

public class WorkActivity extends AppCompatActivity {
    public static String Server_Id = "ServerId";
    public static String SERVER_ADRESS = "ServerAdress";
    public static String SERVER_PORT = "ServerPort";
    public static String Fragment_Pos = "Fragment";
    public static final String CELSIUS = " \u2103";
    private ArrayList<android.support.v4.app.Fragment> fragments;
    private ViewPager viewPager;
    private Database db;
    private long id;
    private int pos = 0;
    private MySwipeFragmentAdapter fa;
    ///since version 0.2
    private String serverAdress;
    private int serverPort;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_work);
        db = Database.getInstance(this);
        // receive server id from intent
        Bundle budle = getIntent().getExtras();
        id = budle.getLong(WorkActivity.Server_Id);
        serverAdress = budle.getString(WorkActivity.SERVER_ADRESS);
        serverPort = budle.getInt(WorkActivity.SERVER_PORT);
        if (savedInstanceState != null)
            viewPager.setCurrentItem(savedInstanceState.getInt(WorkActivity.Fragment_Pos));

        ///


    }

    @Override
    protected void onStart() {
        super.onStart();
        fragments = new ArrayList<android.support.v4.app.Fragment>();
        fragments.add(new NotesFragment());
        fragments.add(new SensorsFragment());
        fragments.add(new ControlFragment());
        fragments.add(new SetingFragment());
        ////
        viewPager = (ViewPager) findViewById(R.id.work_fragments_view_pager);
        fa = new MySwipeFragmentAdapter(getSupportFragmentManager(), fragments);
        viewPager.setAdapter(fa);

    }

    @Override
    protected void onPause() {
        super.onPause();
        pos = viewPager.getCurrentItem();
    }

    @Override
    protected void onResume() {
        super.onResume();
        viewPager.setCurrentItem(pos);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(WorkActivity.Fragment_Pos, viewPager.getCurrentItem());
    }

    public Database getDatabase() {
        return db;
    }

    public long getServerId() {
        return this.id;
    }
    //since version 0.2
    public String getServerAdress()
    {
        return this.serverAdress;
    }
    public  int getServerPort()
    {
        return this.serverPort;
    }
    public void notifyDataChenge(final RecyclerView.Adapter adapter)
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.notifyDataSetChanged();
            }
        });
    }
}