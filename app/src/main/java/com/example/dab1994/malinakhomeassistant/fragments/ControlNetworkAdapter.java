package com.example.dab1994.malinakhomeassistant.fragments;

import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

import com.example.dab1994.malinakhomeassistant.R;
import com.example.dab1994.malinakhomeassistant.WorkActivity;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import network.Control;
import network.inteface.NetControl;
import network.object.NetLightControl;
import network.object.NetRadiatorControl;

/**
 * Created by dab1994 on 23.04.17.
 */

public class ControlNetworkAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
     ///since version 0.2
    public static String LIGHT_ON = "ON";
    public static String LIGHT_OFF = "OFF";
    private ArrayList<NetControl> netControlList;
    private String address;
    private int port;

    /**
     * Type for control_temp.xml
     */
    public static final int TEMP = 0;
    /**
     * Type for control_switch.xml
     */
    public static final int SWITCH = 1;
    /**
     * Type for control_led.xml
     */
    public static final int RGB_LED = 2;

    public ControlNetworkAdapter(String address,int port) {
        //since version 0.2
        this.address=address;
        this.port=port;
        new ReadControl().start();
        while (netControlList==null){}
    }

    /**
     * Create view dependent on view type
     *
     * @param parent
     * @param viewType
     * @return View associated with type or null
     */
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case SWITCH: {
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.control_switch, parent, false);
                return new SwitchViewHolder(view);
            }
            case TEMP: {
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.control_temp, parent, false);
                return new TempViewHolder(view);
            }
            case RGB_LED: {
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.control_led, parent, false);
                return new LEDViewHolder(view);
            }
        }
        return null;
    }

    /**
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case SWITCH: {
                ((SwitchViewHolder) holder).nameTextView.setText(netControlList.get(position).getName());
             //   ((NetLightControl)netControlList.get(position)).getState()
                if(((NetLightControl)netControlList.get(position)).getState()) {
                    ((SwitchViewHolder) holder).text.setText(LIGHT_ON);
                    ((SwitchViewHolder) holder).text.setBackgroundResource(R.color.colorRGBG);
                }
                else {
                    ((SwitchViewHolder) holder).text.setText(LIGHT_OFF);
                    ((SwitchViewHolder) holder).text.setBackgroundResource(R.color.colorCardViewBackground);

                }
                //((SwitchViewHolder) holder).cardView.setOnClickListener(switchCL);
                break;
            }
            case TEMP: {
                ((TempViewHolder) holder).nameTextView.setText(netControlList.get(position).getName());
               // ((NetRadiatorControl)netControlList.get(position)).getTemperature()
                ((TempViewHolder) holder).tempTextView.setText(Double.toString(((NetRadiatorControl)netControlList.get(position)).getTemperature()) + WorkActivity.CELSIUS);
                ((TempViewHolder) holder).tempSeekBar.setProgress((int) ((NetRadiatorControl)netControlList.get(position)).getTemperature());
               // ((TempViewHolder) holder).tempSeekBar.setOnSeekBarChangeListener(new TempChangeListener(((TempViewHolder) holder).cardView));
               ((TempViewHolder) holder).setSwitch(((NetRadiatorControl)netControlList.get(position)).isHeating());
                break;
            }
            case RGB_LED: {
              //  ((LEDViewHolder) holder).nameTextView.setText(names[position]);
              ///  int[] randLED = {rand.nextInt(255), rand.nextInt(255), rand.nextInt(255)};
              //  ((LEDViewHolder) holder).colorCardView.setCardBackgroundColor(Color.rgb(randLED[0], randLED[1], randLED[2]));
             //   ((LEDViewHolder) holder).RSeekBar.setProgress(randLED[0]);
              //  ((LEDViewHolder) holder).GSeekBar.setProgress(randLED[1]);
              //  ((LEDViewHolder) holder).BSeekBar.setProgress(randLED[2]);
                LEDChangeListener ledCL = new LEDChangeListener(((LEDViewHolder) holder).colorCardView, ((LEDViewHolder) holder).RSeekBar, ((LEDViewHolder) holder).GSeekBar, ((LEDViewHolder) holder).BSeekBar);
                ((LEDViewHolder) holder).RSeekBar.setOnSeekBarChangeListener(ledCL);
                ((LEDViewHolder) holder).GSeekBar.setOnSeekBarChangeListener(ledCL);
                ((LEDViewHolder) holder).BSeekBar.setOnSeekBarChangeListener(ledCL);
                break;
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        if(netControlList.get(position) instanceof NetLightControl) return SWITCH;
        if(netControlList.get(position) instanceof NetRadiatorControl) return TEMP;
        return -1;
    }

    @Override
    public int getItemCount() {
        return netControlList.size();
    }

    /**
     * ViewHolder for control_temp
     *
     * @see RecyclerView.ViewHolder
     */
    public class TempViewHolder extends RecyclerView.ViewHolder {
        private TextView nameTextView, tempTextView;
        private SeekBar tempSeekBar;
        private CardView cardView;
        private Switch tempSwitch;

          public void setSwitch(boolean isChecked)
        {
            tempSwitch.setChecked(isChecked);
            if (isChecked)
                cardView.setBackgroundResource(R.color.colorRGBG);
            else
                cardView.setBackgroundResource(R.color.colorCardViewBackground);
            tempSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                    if (isChecked)
                        cardView.setBackgroundResource(R.color.colorRGBG);
                    else
                        cardView.setBackgroundResource(R.color.colorCardViewBackground);
                    new SwitchRadiator( nameTextView.getText().toString()).start();
                }
            });
        }
        public TempViewHolder(View view) {
            super(view);
            nameTextView = (TextView) view.findViewById(R.id.control_temp_name);
            tempTextView = (TextView) view.findViewById(R.id.control_temp_celsius);
            tempSeekBar = (SeekBar) view.findViewById(R.id.control_temp_seek);
            tempSwitch = (Switch) view.findViewById(R.id.control_temp_switch);
            cardView = (CardView) view;
          //  this.value = (TextView) this.v.findViewById(R.id.control_temp_celsius);
           // this.aSwitch = (Switch) v.findViewById(R.id.control_temp_switch);
            tempSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                private int temp;
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                    tempTextView.setText(Integer.toString(progress) + WorkActivity.CELSIUS);
                    temp=progress;
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                    cardView.setBackgroundResource(R.color.colorCardViewBackground);
                    tempSwitch.setChecked(false);
                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    new AdjustRadiator(nameTextView.getText().toString(),(double) temp).start();
                }
            });

        }
    }
    //ViewHolders

    /**
     * ViewHolder for control_switch
     *
     * @see RecyclerView.ViewHolder
     */
    public class SwitchViewHolder extends RecyclerView.ViewHolder {
        private TextView nameTextView,text;
        private CardView cardView;

        /**
         * Create ViewHolder for control_switch
         *
         * @param view
         */
        public SwitchViewHolder(View view) {

            super(view);
            nameTextView = (TextView) view.findViewById(R.id.control_switch_name);
            text = (TextView) view.findViewById(R.id.control_switch_state);
            cardView = (CardView) view;
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if ( text.getText().equals(LIGHT_ON)) {
                        text.setText(LIGHT_OFF);
                        text.setBackgroundResource(R.color.colorCardViewBackground);
                    } else {
                        text.setText(LIGHT_ON);
                        text.setBackgroundResource(R.color.colorRGBG);
                    }
                    new SwitchLight(nameTextView.getText().toString()).start();
                }
            });
        }
    }

    /**
     * ViewHolder for control_led
     *
     * @see RecyclerView.ViewHolder
     */
    public class LEDViewHolder extends RecyclerView.ViewHolder {
        private TextView nameTextView;
        private SeekBar RSeekBar, GSeekBar, BSeekBar;
        private CardView colorCardView;

        /**
         * Create ViewHolder for control_led
         *
         * @param view
         */
        public LEDViewHolder(View view) {
            super(view);
            nameTextView = (TextView) view.findViewById(R.id.control_led_name);
            colorCardView = (CardView) view.findViewById(R.id.control_led_color_card);
            RSeekBar = (SeekBar) view.findViewById(R.id.control_led_R);
            GSeekBar = (SeekBar) view.findViewById(R.id.control_led_G);
            BSeekBar = (SeekBar) view.findViewById(R.id.control_led_B);
        }
    }

    // action listerners


    private class SwitchCheckListener implements CompoundButton.OnCheckedChangeListener {
        private CardView v;

        public SwitchCheckListener(CardView view) {
            this.v = view;
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked)
                v.setBackgroundResource(R.color.colorRGBG);
            else
                v.setBackgroundResource(R.color.colorCardViewBackground);
        }
    }

    private class LEDChangeListener implements SeekBar.OnSeekBarChangeListener {
        private CardView cardRGB;
        private SeekBar rBar, gBar, bBar;

        public LEDChangeListener(CardView view, SeekBar r, SeekBar g, SeekBar b) {
            this.cardRGB = view;
            this.rBar = r;
            this.gBar = g;
            this.bBar = b;
        }

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            switch (seekBar.getId()) {
                case R.id.control_led_R: {
                    cardRGB.setCardBackgroundColor(Color.rgb(progress, gBar.getProgress(), bBar.getProgress()));
                    break;
                }
                case R.id.control_led_G: {
                    cardRGB.setCardBackgroundColor(Color.rgb(rBar.getProgress(), progress, bBar.getProgress()));
                    break;
                }
                case R.id.control_led_B: {
                    cardRGB.setCardBackgroundColor(Color.rgb(rBar.getProgress(), gBar.getProgress(), progress));
                    break;
                }
            }
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            //value.setText(Integer.toString(seekBar.getProgress()));
        }
    }
    class ReadControl extends Thread {
        private Socket socket = null;
        private ObjectOutputStream obOut;
        private ObjectInputStream obIn;
        private Control cmd;

        @Override
        public void run() {
            try {
                Log.i("ReadControl", "Run start");
                /// open connection and innit stream
                this.socket = new Socket(address, port);
                this.obOut = new ObjectOutputStream(socket.getOutputStream());
                this.obIn = new ObjectInputStream(socket.getInputStream());
                /// send command receive sensor state from server
                cmd = new Control();
                cmd.setCommand(Control.READ_CONTROLS);
                this.obOut.writeObject(cmd);
                this.obOut.flush();
                /// read sensor state
                netControlList = (ArrayList<NetControl>) this.obIn.readObject();
                Log.i("ReadControl", ""+netControlList.size());

                ///send close command to server
                cmd = new Control();
                cmd.setCommand(Control.CLOSE);
                this.obOut.writeObject(cmd);
                this.obOut.flush();

                Log.i("ReadControl", "Run end");

            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    } /// read control thread
    class SwitchLight extends Thread{
        private Socket socket = null;
        private ObjectOutputStream obOut;
        private ObjectInputStream obIn;
        private Control cmd;
        private String lightName;
        SwitchLight(String lightName)
        {
            this.lightName=lightName;
        }

        @Override
        public void run() {
            try {
                Log.i("SwitchLight", "Run start");
                /// open connection and innit stream
                this.socket = new Socket(address, port);
                this.obOut = new ObjectOutputStream(socket.getOutputStream());
                this.obIn = new ObjectInputStream(socket.getInputStream());
                /// send command switch light
                cmd = new Control();
                cmd.setCommand(Control.SWITCH_LIGHT);
                this.obOut.writeObject(cmd);
                this.obOut.writeUTF(this.lightName);
                this.obOut.flush();
                ///send close command to server
                cmd = new Control();
                cmd.setCommand(Control.CLOSE);
                this.obOut.writeObject(cmd);
                this.obOut.flush();

                Log.i("ReadControl", "Run end");

            } catch (IOException e) {
                e.printStackTrace();

            }
        }
    }//Switch Light Thread
    class SwitchRadiator extends Thread{
    private Socket socket = null;
    private ObjectOutputStream obOut;
    private ObjectInputStream obIn;
    private Control cmd;
    private String radiatorName;
    SwitchRadiator(String radiatorName)
    {
        this.radiatorName=radiatorName;
    }

    @Override
    public void run() {
        try {
            Log.i("SwitchRadiator", "Run start");
            /// open connection and innit stream
            this.socket = new Socket(address, port);
            this.obOut = new ObjectOutputStream(socket.getOutputStream());
            this.obIn = new ObjectInputStream(socket.getInputStream());
            /// send command switch radiator
            cmd = new Control();
            cmd.setCommand(Control.SWITCH_RADIATOR);
            this.obOut.writeObject(cmd);
            this.obOut.writeUTF(this.radiatorName);
            this.obOut.flush();
            ///send close command to server
            cmd = new Control();
            cmd.setCommand(Control.CLOSE);
            this.obOut.writeObject(cmd);
            this.obOut.flush();

            Log.i("SwitchRadiator", "Run end");

        } catch (IOException e) {
            e.printStackTrace();

        }
    }
}//Switch Radiator Thread
    class AdjustRadiator extends Thread{
        private Socket socket = null;
        private ObjectOutputStream obOut;
        private ObjectInputStream obIn;
        private Control cmd;
        private String radiatorName;
        private double temperature;
        AdjustRadiator(String radiatorName, double temperature)
        {
            this.radiatorName=radiatorName;
            this.temperature=temperature;
        }

        @Override
        public void run() {
            try {
                Log.i("SwitchRadiator", "Run start");
                /// open connection and innit stream
                this.socket = new Socket(address, port);
                this.obOut = new ObjectOutputStream(socket.getOutputStream());
                this.obIn = new ObjectInputStream(socket.getInputStream());
                /// send command adjust radiator
                cmd = new Control();
                cmd.setCommand(Control.ADJUST_RADIATOR);
                this.obOut.writeObject(cmd);
                this.obOut.writeUTF(this.radiatorName);
                this.obOut.writeDouble(this.temperature);
                this.obOut.flush();
                ///send close command to server
                cmd = new Control();
                cmd.setCommand(Control.CLOSE);
                this.obOut.writeObject(cmd);
                this.obOut.flush();

                Log.i("SwitchRadiator", "Run end");

            } catch (IOException e) {
                e.printStackTrace();

            }
        }
    }//Adjust Radiator Thread
}