package com.example.dab1994.malinakhomeassistant.fragments;

import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

import com.example.dab1994.malinakhomeassistant.R;
import com.example.dab1994.malinakhomeassistant.WorkActivity;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import network.Control;
import network.inteface.NetControl;
import network.inteface.NetSensor;
import network.object.NetDoorSensor;
import network.object.NetLightControl;
import network.object.NetRadiatorControl;
import network.object.NetRainSensor;
import network.object.NetTemperatureSensor;

/**
 * Created by dab1994 on 23.04.17.
 */

public class SensorNetworkAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
     ///since version 0.2
    private String address;
    private int port;
    private WorkActivity workActivity;
    public static String DOOR_CLOSE = "CLOSE";
    public static String DOOR_OPEN = "OPEN";
    public static String RAIN_DETECTED = "DETECTED";
    public static String RAIN_NO_DETECTED = "NO DETECTED";
    /**
     * Type for sensor_door.xml
     */
    public static final int DOOR = 0;
    /**
     * Type for sensor_rain.xml
     */
    public static final int RAIN = 1;
    /**
     * Type for sensor_temp.xml
     */
    public static final int TEMP = 2;
    private ArrayList<NetSensor> sensors;
    private ScheduledExecutorService updateService;

    public SensorNetworkAdapter(WorkActivity workActivity) {
        //since version 0.2
        this.address=workActivity.getServerAdress();
        this.port=workActivity.getServerPort();
        this.workActivity=workActivity;
        new ReadSensor().start();
        sensors = new ArrayList<NetSensor>();
        while (sensors.size()==0){}
        updateService =Executors.newSingleThreadScheduledExecutor();
        updateService.scheduleAtFixedRate(new UpdateSensor(),10,10, TimeUnit.SECONDS);
    }

    /**
     * Create view dependent on view type
     *
     * @param parent
     * @param viewType
     * @return View associated with type or null
     */
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case DOOR: {
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.sensor_door, parent, false);
                return new DoorViewHolder(view);
            }
            case RAIN: {
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.sensor_rain, parent, false);
                return new RainViewHolder(view);
            }
            case TEMP: {
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.sensor_temp, parent, false);
                return new TempViewHolder(view);
            }
        }
        return null;
    }

    /**
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case DOOR: {
                ((DoorViewHolder) holder).nameTextView.setText(sensors.get(position).getName());
                if(((NetDoorSensor)sensors.get(position)).getState()) {
                    ((DoorViewHolder) holder).stateTextView.setText(DOOR_OPEN);
                    ((DoorViewHolder) holder).stateTextView.setBackgroundResource(R.color.colorRGBG);
                }
                else {
                    ((DoorViewHolder) holder).stateTextView.setText(DOOR_CLOSE);
                    ((DoorViewHolder) holder).stateTextView.setBackgroundResource(R.color.colorCardViewBackground);
                }

                break;
            }
            case RAIN: {
                ((RainViewHolder) holder).nameTextView.setText(sensors.get(position).getName());
                if(((NetRainSensor)sensors.get(position)).getState()) {
                    ((RainViewHolder) holder).stateTextView.setText(RAIN_DETECTED);
                    ((RainViewHolder) holder).stateTextView.setBackgroundResource(R.color.colorRainDetected);
                }
                else {
                    ((RainViewHolder) holder).stateTextView.setText(RAIN_NO_DETECTED);
                    ((RainViewHolder) holder).stateTextView.setBackgroundResource(R.color.colorCardViewBackground);
                }
                break;
            }
            case TEMP :
                ((TempViewHolder) holder).nameTextView.setText(sensors.get(position).getName());
                double t = ((NetTemperatureSensor)sensors.get(position)).getValue();
                DecimalFormat df = new DecimalFormat("#.##");
                ((TempViewHolder) holder).tempTextView.setText(df.format(t).toString() + WorkActivity.CELSIUS);
                ((TempViewHolder) holder).tempTextView.setBackgroundResource(R.color.colorCardViewBackground);
                break;
            }
        }

    @Override
    public int getItemViewType(int position) {
        if (sensors.get(position) instanceof NetDoorSensor) return DOOR;
        if (sensors.get(position) instanceof NetRainSensor) return RAIN;
        if (sensors.get(position) instanceof NetTemperatureSensor) return TEMP;
        return -1;
    }

    @Override
    public int getItemCount() {
        return sensors.size();
    }
    public void notifyDataChange()
    {
        workActivity.notifyDataChenge(this);
    }
    public void shutdown()
    {
        updateService.shutdown();
    }

    public class DoorViewHolder extends RecyclerView.ViewHolder {
        private TextView nameTextView, stateTextView;

        public DoorViewHolder(View itemView) {
            super(itemView);
            nameTextView = (TextView) itemView.findViewById(R.id.door_sensor_name);
            stateTextView = (TextView) itemView.findViewById(R.id.door_sensor_state);
        }
    }

    public class RainViewHolder extends RecyclerView.ViewHolder {
        private TextView nameTextView, stateTextView;

        public RainViewHolder(View itemView) {
            super(itemView);
            nameTextView = (TextView) itemView.findViewById(R.id.rain_sensor_name);
            stateTextView = (TextView) itemView.findViewById(R.id.rain_sensor_state);
        }
    }
    public class TempViewHolder extends RecyclerView.ViewHolder {
        private TextView nameTextView, tempTextView;

        public TempViewHolder(View itemView) {
            super(itemView);
            nameTextView = (TextView) itemView.findViewById(R.id.temperature_sensor_name);
            tempTextView = (TextView) itemView.findViewById(R.id.temperature_sensor_state);
        }
    }

    class ReadSensor extends Thread {
        private Socket socket = null;
        private ObjectOutputStream obOut;
        private ObjectInputStream obIn;
        private Control cmd;

        @Override
        public void run() {
            try {
                Log.i("ReadSensor", "Run start");
                /// open connection and innit stream
                this.socket = new Socket(address, port);
                this.obOut = new ObjectOutputStream(socket.getOutputStream());
                this.obIn = new ObjectInputStream(socket.getInputStream());
                /// send command receive sensor state from server
                cmd = new Control();
                cmd.setCommand(Control.READ_SENSORS);
                this.obOut.writeObject(cmd);
                this.obOut.flush();
                /// read sensor state
                ArrayList<NetSensor> list = (ArrayList<NetSensor>) this.obIn.readObject();
                sensors.addAll(list);
                ///send close command to server
                cmd = new Control();
                cmd.setCommand(Control.CLOSE);
                this.obOut.writeObject(cmd);
                this.obOut.flush();

                Log.i("ReadSensor", "Run end");

            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    } //ReadSensor
    class UpdateSensor extends Thread {
        private Socket socket = null;
        private ObjectOutputStream obOut;
        private ObjectInputStream obIn;
        private Control cmd;

        @Override
        public void run() {
            try {
                Log.i("UpdateSensor", "Run start");
                /// open connection and innit stream
                this.socket = new Socket(address, port);
                this.obOut = new ObjectOutputStream(socket.getOutputStream());
                this.obIn = new ObjectInputStream(socket.getInputStream());
                /// send command receive sensor state from server
                cmd = new Control();
                cmd.setCommand(Control.READ_SENSORS);
                this.obOut.writeObject(cmd);
                this.obOut.flush();
                /// read sensor state
                ArrayList<NetSensor> list = (ArrayList<NetSensor>) this.obIn.readObject();
                sensors.clear();
                sensors.addAll(list);
                notifyDataChange();
              ///send close command to server
                cmd = new Control();
                cmd.setCommand(Control.CLOSE);
                this.obOut.writeObject(cmd);
                this.obOut.flush();

                Log.i("UpdateSensor", "Run end");

            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    } //ReadSensor
}
