package com.example.dab1994.malinakhomeassistant.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.dab1994.malinakhomeassistant.R;
import com.example.dab1994.malinakhomeassistant.WorkActivity;
import com.example.dab1994.malinakhomeassistant.bindActivity.ControlBind;
import com.example.dab1994.malinakhomeassistant.bindActivity.SensorBind;

/**
 * A simple {@link Fragment} subclass.
 */
public class ControlFragment extends Fragment {
    private RecyclerView controlRecyclerView;
    private RecyclerView.Adapter controlAdapter;
    private RecyclerView.LayoutManager controlLayoutManager;
    private FloatingActionButton newControlShowButton;
    public ControlFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_control,container,false);
        controlRecyclerView = (RecyclerView) view.findViewById(R.id.control_view);
        controlLayoutManager = new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL);
        controlRecyclerView.setLayoutManager(controlLayoutManager);
        controlRecyclerView.setItemAnimator(new DefaultItemAnimator());
        newControlShowButton = (FloatingActionButton) view.findViewById(R.id.control_new_show);
        newControlShowButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ControlBind.class));
            }
        });
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
       // controlAdapter = new ControlAdapter(new String[]{"Light01","Light02","Light03","Light04","Light05","Radiator01","Radiator02","Water01","LED01"},
       //         new Integer[]{ControlAdapter.SWITCH,ControlAdapter.SWITCH,ControlAdapter.SWITCH,ControlAdapter.SWITCH,ControlAdapter.SWITCH,ControlAdapter.TEMP,ControlAdapter.TEMP,ControlAdapter.TEMP,ControlAdapter.RGB_LED});
        controlAdapter = new ControlNetworkAdapter(((WorkActivity)getActivity()).getServerAdress(),((WorkActivity)getActivity()).getServerPort());
       // ((WorkActivity)getActivity()).getServerPort()
        controlRecyclerView.setAdapter(controlAdapter);
    }
}
