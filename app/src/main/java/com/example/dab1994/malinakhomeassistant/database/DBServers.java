package com.example.dab1994.malinakhomeassistant.database;

import android.provider.BaseColumns;

/**
 * Created by dab1994 on 05.04.17.
 */

public final class DBServers implements BaseColumns {
    // public static final String ="";
    //Database column, servers list table
    public static final String TABLE_NAME = "ServersList";
    public static final String ID_COLUMN = "ServerId";
    public static final String USER_COLUMN = "user";
    public static final String SERVER_ADDRESS_COLUMN = "address";
    public static final String SERVER_PORT_COLUMN = "port";
    public static final String PASSWORD_COLUMN = "password";

    /**
     * Create table query
     */
    public static final String CREATE_SERVERS_LIST =
            "CREATE TABLE " + DBServers.TABLE_NAME + " (" + DBServers.ID_COLUMN + " INTEGER PRIMARY KEY AUTOINCREMENT , "
                    + DBServers.SERVER_ADDRESS_COLUMN + " TEXT, " + DBServers.SERVER_PORT_COLUMN + " TEXT, "
                    + DBServers.USER_COLUMN + " TEXT, " + DBServers.PASSWORD_COLUMN + " TEXT)";

    /**
     * Drop table query
     */
    public static final String DROP_SERVERS_LIST = "DROP TABLE IF EXISTS " + DBServers.TABLE_NAME;
    /**
     * Delete clause for delete row using his id
     */
    public static final String DELETE_CLAUSE = DBServers.ID_COLUMN + " = ?";
    /**
     * sql statement using to get user name from database
     */
    public static final String GET_USER_NAME = DBServers.ID_COLUMN + " = ?";
    /**
     * sql statement select all columns from table, return fully list saves servers conf
     */
    public static final String SELECT_ALL = "SELECT "+DBServers.ID_COLUMN+ " AS _id, "+DBServers.SERVER_ADDRESS_COLUMN+ ", "+DBServers.SERVER_PORT_COLUMN+ ", " +DBServers.USER_COLUMN+ ", "+DBServers.PASSWORD_COLUMN +" FROM " + DBServers.TABLE_NAME;

}