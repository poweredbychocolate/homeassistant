package com.example.dab1994.malinakhomeassistant;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dab1994.malinakhomeassistant.database.DBServers;
import com.example.dab1994.malinakhomeassistant.database.Database;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;

import network.Control;


public class StartActivity extends AppCompatActivity {
    private FloatingActionButton fab;
    private Database db;
    //  private TextView serversListTextView;
    /// private ArrayList<ServerInfo> serversList;
    CursorAdapter serverListAdapter;
    //// since version 0.2

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fab = (FloatingActionButton) findViewById(R.id.add_server);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(StartActivity.this, LoginActivity.class));
            }
        });
        db = Database.getInstance(this);
        //// @ for version 0.2 net communication
    }

    @Override
    protected void onStart() {
        super.onStart();
                /*
        serversListTextView  = (TextView) findViewById(R.id.lista_start);
        serversList = db.getServerList();

        String list = new String();
        for(ServerInfo s :serversList)
        {
            list=list+s.toString()+" \n";
        }
        serversListTextView.setText(list);
        */
        String[] usingColummn = new String[]{
                DBServers.SERVER_ADDRESS_COLUMN,
                DBServers.SERVER_PORT_COLUMN
        };
        int[] textViewIds = new int[]{
                R.id.single_server_address,
                R.id.single_server_port
        };
        serverListAdapter = new SimpleCursorAdapter(this, R.layout.single_server, db.getCursor(Database.SERVER_LIST_CURSOR), usingColummn, textViewIds, 2);
        ListView serversView = (ListView) findViewById(R.id.servers_list_view);
        serversView.setAdapter(serverListAdapter);
        Log.i("Adapter cursor size ", Integer.toString(serverListAdapter.getCount()));

       /* AdapterView.OnItemClickListener serversListAV = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent start = new Intent(StartActivity.this, WorkActivity.class);
                start.putExtra(WorkActivity.Server_Id, id);
                startActivity(start);
            }
        };
        */
        AdapterView.OnItemClickListener serversListAV = new AdapterView.OnItemClickListener() {
            long tmpid;
            TextView adress;
            TextView port;
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
               adress = (TextView) view.findViewById(R.id.single_server_address);
                port = (TextView) view.findViewById(R.id.single_server_port);
                tmpid=id;
                Log.i("Adapter Test",adress.getText().toString());
                Log.i("Adapter Test",adress.getText().toString());
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Socket socket = new Socket(adress.getText().toString(),Integer.parseInt(port.getText().toString()));
                             ObjectOutputStream obOut = new ObjectOutputStream(socket.getOutputStream());
                            Control cmd = new Control();
                            cmd.setCommand(Control.CLOSE);
                            obOut.writeObject(cmd);
                            obOut.flush();
                            Intent start = new Intent(StartActivity.this, WorkActivity.class);
                            start.putExtra(WorkActivity.Server_Id, tmpid);
                            start.putExtra(WorkActivity.SERVER_ADRESS,adress.getText().toString());
                            start.putExtra(WorkActivity.SERVER_PORT, Integer.parseInt(port.getText().toString()));
                            startActivity(start);
                            Log.i("Sever Test","available");
                            showToast("Connect with server");
                        } catch (IOException e) {
                            Log.i("Sever Test"," no available");
                            showToast("Server is not available");
                        }

                    }
                }).start();
                ////

            }
        };
        AdapterView.OnItemLongClickListener serverDeleteAV = new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                if (db.deleteServer(id) > 0) {
                   Toast.makeText(getApplicationContext(),"Server deleted ",Toast.LENGTH_LONG).show();
                   serverListAdapter.swapCursor(db.getCursor(Database.SERVER_LIST_CURSOR));
                    return true;
                }
                return false;
            }
        };
        serversView.setOnItemClickListener(serversListAV);
        serversView.setOnItemLongClickListener(serverDeleteAV);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.menu_start, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        serverListAdapter.getCursor().close();
        super.onDestroy();
    }
    public void showToast(final String content)
    {
        runOnUiThread(new Runnable() {
            public void run()
            {
                Toast.makeText(StartActivity.this,content, Toast.LENGTH_LONG).show();
            }
        });
    }
}
