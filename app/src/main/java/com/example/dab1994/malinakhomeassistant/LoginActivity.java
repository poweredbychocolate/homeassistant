package com.example.dab1994.malinakhomeassistant;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.app.LoaderManager.LoaderCallbacks;

import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.dab1994.malinakhomeassistant.database.Database;

import java.util.ArrayList;
import java.util.List;

import static android.Manifest.permission.READ_CONTACTS;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {
     // UI references.
    private EditText mServerAddressView;
    private EditText mServerPortView;
    private EditText mServerUserView;
    private EditText mServerPasswordView;
    private View mProgressView;
    private View mLoginFormView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setupActionBar();
        // Set up the login form.
        mServerAddressView = (EditText) findViewById(R.id.server_address);
        mServerPortView = (EditText) findViewById(R.id.server_port);
        mServerUserView = (EditText) findViewById(R.id.server_user);
        mServerPasswordView = (EditText) findViewById(R.id.server_password);
        Button mAddServerButton = (Button) findViewById(R.id.server_add_button);
        OnClickListener addButtonListener = new OnClickListener() {
            @Override
            public void onClick(View v) {
              attemptLogin();

            }
        };
        mAddServerButton.setOnClickListener(addButtonListener);
        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
    }

    /**
     * Set up the {@link android.app.ActionBar}, if the API is available.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void setupActionBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            // Show the Up button in the action bar.
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        // Reset errors.
        mServerAddressView.setError(null);
        mServerPortView.setError(null);
        mServerUserView.setError(null);
        mServerPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String sUser = mServerUserView.getText().toString();
        String sAddress = mServerAddressView.getText().toString();
        String sPort = mServerPortView.getText().toString();
        String sPassword = mServerPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(sPassword) && !isPasswordValid(sPassword)) {
            mServerPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mServerPasswordView;
            cancel = true;
        }
        // Check for a valid email address.
        if (TextUtils.isEmpty(sAddress)) {
            mServerAddressView.setError(getString(R.string.error_field_required));
            focusView = mServerAddressView;
            cancel = true;
        } else if (!isEmailValid(sAddress)) {
            mServerAddressView.setError(getString(R.string.error_invalid_email));
            focusView = mServerAddressView;
            cancel = true;
        }
        if (TextUtils.isEmpty(sPort)) {
            mServerAddressView.setError(getString(R.string.error_field_required));
            focusView = mServerPortView;
            cancel = true;
        }
        if (TextUtils.isEmpty(sUser)) {
            mServerAddressView.setError(getString(R.string.error_field_required));
            focusView = mServerUserView;
            cancel = true;
        }
            if (cancel) {
                Log.i("No","No");
                // There was an error; don't attempt login and focus the first
                // form field with an error.
                focusView.requestFocus();
              //  saveServerConfig(sAddress,sPort,sUser,sPassword);
            } else {
                Log.i("Yes","Yes");
                // Show a progress spinner, and kick off a background task to
                // perform the user login attempt.
               showProgress(true);
                saveServerConfig(sAddress,sPort,sUser,sPassword);
                finish();
               // backToParentActivity();
                // mAuthTask = new UserLoginTask(email, password);
                // mAuthTask.execute((Void) null);
            }

     }

    private void backToParentActivity()
    {
        this.finish();
    }
    private boolean isEmailValid(String address) {
        //TODO: Replace this with your own logic
        return address.contains(".");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 6;
    }
    private void saveServerConfig(String address, String port, String login, String pass)
    {
        Log.i("ADD","ADD");
        Database db = Database.getInstance(this);
        db.insertServer(address,port,login,pass);
       // db.initSettings();
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

}

