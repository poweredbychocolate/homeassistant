package com.example.dab1994.malinakhomeassistant.fragments;

import android.database.Cursor;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dab1994.malinakhomeassistant.R;
import com.example.dab1994.malinakhomeassistant.database.DBNotes;
import com.example.dab1994.malinakhomeassistant.database.Database;

/**
 * Created by dab1994 on 19.04.17.
 */

public class NotesAdapter extends RecyclerView.Adapter<NotesAdapter.ViewHolder> {
    private Cursor nCursor;
    private NotesAdapter nA;

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView authTextView, contTextView;
        private View view;

        // each data item is just a string in this case
        public ViewHolder(View view) {
            super(view);
            this.view = view;
            authTextView = (TextView) view.findViewById(R.id.note_author);
            contTextView = (TextView) view.findViewById(R.id.note_content);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public NotesAdapter(Cursor notesCursor) {
        nCursor = notesCursor;
        nA = this;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.note_view, parent, false);
        return new ViewHolder(itemView);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        nCursor.moveToPosition(position);
        final long id = nCursor.getLong(nCursor.getColumnIndex(DBNotes._ID));
        String author = nCursor.getString(nCursor.getColumnIndex(DBNotes.AUTHOR));
        int status = nCursor.getInt(nCursor.getColumnIndex(DBNotes.STATUS));
        if (status == DBNotes.READ) {
            holder.authTextView.setBackgroundResource(R.color.colorReadNotes);
        } else if (author.equals("Arduino"))
            holder.authTextView.setBackgroundResource(R.color.colorArduinoNotes);
        else holder.authTextView.setBackgroundResource(R.color.colorAccent);


        holder.authTextView.setText(author);
        holder.contTextView.setText(nCursor.getString(nCursor.getColumnIndex(DBNotes.CONTENTS)));
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Database.getInstance(holder.view.getContext()).changeStatus(id, 1) > 0) {
                    Toast.makeText(v.getContext(),R.string.notes_read_notes , Toast.LENGTH_LONG).show();
                    nCursor = Database.getInstance(v.getContext()).getCursor(Database.NOTES_CURSOR);
                    nA.notifyDataSetChanged();
                }
            }
        });
        holder.view.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (Database.getInstance(holder.view.getContext()).deleteNote(id) > 0) {
                    Toast.makeText(v.getContext(),R.string.setting_clear_notes, Toast.LENGTH_LONG).show();
                    nCursor = Database.getInstance(v.getContext()).getCursor(Database.NOTES_CURSOR);
                    nA.notifyDataSetChanged();
                    return true;
                }
                return false;
            }
        });

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return nCursor.getCount();
    }
}
