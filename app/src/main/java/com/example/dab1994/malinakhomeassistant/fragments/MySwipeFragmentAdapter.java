package com.example.dab1994.malinakhomeassistant.fragments;

import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

/**
 * Created by dab1994 on 17.04.17.
 */

public class MySwipeFragmentAdapter extends FragmentStatePagerAdapter {
    private ArrayList<Fragment> fragments;
    private int poss;
    public MySwipeFragmentAdapter(FragmentManager fm,ArrayList<Fragment> flist) {
        super(fm);
        fragments = new ArrayList<android.support.v4.app.Fragment>();
        fragments.addAll(flist);
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }
}
