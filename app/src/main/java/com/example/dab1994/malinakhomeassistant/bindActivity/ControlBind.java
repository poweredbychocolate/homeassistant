package com.example.dab1994.malinakhomeassistant.bindActivity;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.example.dab1994.malinakhomeassistant.R;

public class ControlBind extends AppCompatActivity {
    private Spinner controlSelected;
    private ArrayAdapter<String> controlAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_control_bind);
       Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
      setSupportActionBar(toolbar);
        controlSelected = (Spinner) findViewById(R.id.control_selected);
        /// !!!! move to onStart
        controlAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,new String[]{"Control01","Control02","Control03","Control04"});
        controlAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        controlSelected.setAdapter(controlAdapter);
        /// !!!! move to onStart
            }

}
