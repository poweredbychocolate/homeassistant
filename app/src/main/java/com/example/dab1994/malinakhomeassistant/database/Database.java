package com.example.dab1994.malinakhomeassistant.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by dab1994 on 05.04.17.
 */

public class Database extends SQLiteOpenHelper {
    private static Database instance=null;
    /**
     * Latest database version
     */
    public static final int DATABASE_VERSION = 1;
    /**
     * Database name
     */
    public static final String DATABASE_NAME = "HomeAssistantDB.db";

    public static final Integer SERVER_LIST_CURSOR= 1;
    public static final Integer NOTES_CURSOR=2 ;
    //public static final Integer = ;

   private Database(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

    }

    /**
     * Static metod return Database object, with create it before first use. Always return same object
     * @param context
     * @return Database
     */
    public static Database getInstance(Context context) {
        if (instance==null) {
            instance = new Database(context);
        }
        return instance;
    }

    @Override
    /**
     * Create all need tables
     */
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(DBServers.CREATE_SERVERS_LIST);
        db.execSQL(DBNotes.CREATE_TABLE);
        db.execSQL(DBSettings.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DBServers.DROP_SERVERS_LIST);
        db.execSQL(DBNotes.DROP);
        db.execSQL(DBSettings.DROP_TABLE);
        onCreate(db);
    }
    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DBServers.DROP_SERVERS_LIST);
        db.execSQL(DBNotes.DROP);
        onCreate(db);
    }
    /**
     * Insert new row consist server access configuration
     * @param serverAdrress IPv4 server address
     * @param serverPort Server port
     * @param userLogin User login on choose server
     * @param userPassword User password on choose server
     * @return long the row ID of the newly inserted row, or -1 if an error occurred
     */
    public long insertServer(String serverAdrress,String serverPort,String userLogin, String userPassword)
    {
        ContentValues serverData = new ContentValues();
        serverData.put(DBServers.SERVER_ADDRESS_COLUMN,serverAdrress);
        serverData.put(DBServers.SERVER_PORT_COLUMN,serverPort);
        serverData.put(DBServers.USER_COLUMN,userLogin);
        serverData.put(DBServers.PASSWORD_COLUMN,userPassword);
       return this.getWritableDatabase().insert(DBServers.TABLE_NAME,null,serverData);
    }

    /**
     * Delete row with server access configuration using his id
     * @param serverId
     */
    public int deleteServer(long serverId){
       return this.getWritableDatabase().delete(DBServers.TABLE_NAME,DBServers.DELETE_CLAUSE,new String[]{Long.toString(serverId)});
    }
    public String getUserName(long id)
    {
        Cursor cursor = this.getReadableDatabase().query(DBServers.TABLE_NAME,new String[]{DBServers.USER_COLUMN},DBServers.GET_USER_NAME,new String[]{Long.toString(id)},null,null,null,null);
        cursor.moveToFirst();
        String str= cursor.getString(cursor.getColumnIndex(DBServers.USER_COLUMN));
        cursor.close();
        return str;
    }
    public Cursor getCursor(Integer cursorType)
    {
        if (cursorType==SERVER_LIST_CURSOR)
        {
            Cursor c= this.getReadableDatabase().rawQuery(DBServers.SELECT_ALL, null );
            Log.i("cursor size",Integer.toString(c.getCount()));
           return c;
        }
        if (cursorType==NOTES_CURSOR)
        {
            Cursor c= this.getReadableDatabase().rawQuery(DBNotes.SELECT_ALL, null );
            Log.i("cursor size",Integer.toString(c.getCount()));
            return c;
        }
        return null;
    }

    /**
     * Insert new note to database
     * @param author
     * @param content
     * @return
     */
    public long insertNote(String author,String content)
    {
        ContentValues data = new ContentValues();
        data.put(DBNotes.AUTHOR,author);
        data.put(DBNotes.CONTENTS,content);
        data.put(DBNotes.STATUS,DBNotes.UNREAD);
        return this.getWritableDatabase().insert(DBNotes.TABLE_NAME,null,data);
    }

    /**
     * Delete note from database
     * @param noteId
     * @return
     */
    public int deleteNote(long noteId){
        return this.getWritableDatabase().delete(DBNotes.TABLE_NAME,DBNotes.DELETE_CLAUSE,new String[]{Long.toString(noteId)});
    }
    public void deleteAllNote()
    {
        this.getWritableDatabase().execSQL(DBNotes.DELETE_ALL);
    }
    public long changeStatus(Long noteId,int status)
    {
        ContentValues data = new ContentValues();
        data.put(DBNotes.STATUS,status);
        return this.getWritableDatabase().update(DBNotes.TABLE_NAME,data,DBNotes.UPDATE_STATUS_CLAUSE,new String[]{Long.toString(noteId)});
    }
/**
 * Set default configuration in setting options fragment
 */
public void initSettings()
{
    ContentValues settings = new ContentValues();
    SQLiteDatabase db = getWritableDatabase();
    /// auto remove old notes setting
    settings.put(DBSettings.TABLE_OPTIONS,DBSettings.NOTE_AUTODELETE);
    settings.put(DBSettings.TABLE_VALUE,DBSettings.DISABLE);
   db.insert(DBSettings.TABLE_NAME,null,settings);
    settings.clear();
    /// visable of note add button
    settings.put(DBSettings.TABLE_OPTIONS,DBSettings.NOTE_VISABLE);
    settings.put(DBSettings.TABLE_VALUE,DBSettings.ENABLE);
    db.insert(DBSettings.TABLE_NAME,null,settings);
    settings.clear();
    /// visable of control add button
    settings.put(DBSettings.TABLE_OPTIONS,DBSettings.CONTROL_VISABLE);
    settings.put(DBSettings.TABLE_VALUE,DBSettings.ON_LEFT);
    db.insert(DBSettings.TABLE_NAME,null,settings);
    settings.clear();
    /// visable of sensor add button
    settings.put(DBSettings.TABLE_OPTIONS,DBSettings.SENSOR_VISABLE);
    settings.put(DBSettings.TABLE_VALUE,DBSettings.ON_LEFT);
    db.insert(DBSettings.TABLE_NAME,null,settings);
    settings.clear();
    Log.i("Init setting ","COMPLETED");
}
public int getOptionsValue(String option)
{
    SQLiteDatabase db = getReadableDatabase();
    Cursor cursor;
    Integer value=-1;
    if (option.equals(DBSettings.NOTE_VISABLE))
    {

    cursor= db.rawQuery(DBSettings.SELECT_NOTE_VISABLE,null);
        Log.i("WADSAFDF",Integer.toString(cursor.getInt(0)));
    }
    return value;
}
}
